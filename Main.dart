import 'Game.dart';

void main(List<String> args) {
  Game game = Game();
  bool isFinish = false;
  game.printGameName();
  while (isFinish != true) {
    game.runGame();
    isFinish = true;
  }
}

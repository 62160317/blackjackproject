import 'dart:io';
import 'Card.dart';
import 'Dealer.dart';
import 'Player.dart';

class Game {
  Card card = Card();
  Dealer dealer = Dealer();
  Player player = Player();

  var nameGame = (r'''
    __     __     ______     __         ______     ______     __    __     ______        ______   ______    
    /\ \  _ \ \   /\  ___\   /\ \       /\  ___\   /\  __ \   /\ "-./  \   /\  ___\      /\__  _\ /\  __ \   
    \ \ \/ ".\ \  \ \  __\   \ \ \____  \ \ \____  \ \ \/\ \  \ \ \-./\ \  \ \  __\      \/_/\ \/ \ \ \/\ \  
    \ \__/".~\_\  \ \_____\  \ \_____\  \ \_____\  \ \_____\  \ \_\ \ \_\  \ \_____\       \ \_\  \ \_____\ 
      \/_/   \/_/   \/_____/   \/_____/   \/_____/   \/_____/   \/_/  \/_/   \/_____/        \/_/   \/_____/ 
                                                                                                                ______     __         ______     ______     __  __       __     ______     ______     __  __            
    /\  == \   /\ \       /\  __ \   /\  ___\   /\ \/ /      /\ \   /\  __ \   /\  ___\   /\ \/ /            
    \ \  __<   \ \ \____  \ \  __ \  \ \ \____  \ \  _"-.   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.          
    \ \_____\  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\ /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\         
      \/_____/   \/_____/   \/_/\/_/   \/_____/   \/_/\/_/ \/_____/   \/_/\/_/   \/_____/   \/_/\/_/         
                                                                                                            
  ''');

  void printGameName() {
    int count = 0;
    while (count < 1) {
      print('\x1B[31m$nameGame\x1B[0m');
      sleep(const Duration(milliseconds: 1000));
      print("\x1B[2J\x1B[0;0H");

      print('\x1B[33m$nameGame\x1B[0m');
      sleep(const Duration(milliseconds: 1000));
      print("\x1B[2J\x1B[0;0H");

      print('\x1B[32m$nameGame\x1B[0m');
      sleep(const Duration(milliseconds: 1000));
      print("\x1B[2J\x1B[0;0H");

      print('\x1B[34m$nameGame\x1B[0m');
      sleep(const Duration(milliseconds: 1000));
      print("\x1B[2J\x1B[0;0H");

      count++;
    }
  }

  void createDealerCard() {
    dealer.point += card.createCard(dealer.dealerCard);
  }

  void createPlayerCard() {
    player.point += card.createCard(player.playerCard);
  }

  void showPlayerpoint() {
    player.showPoint();
  }

  void showDealerPoint() {
    dealer.showPoint();
  }

  void showPlayerCard() {
    player.printCard();
  }

  void showDealerCard() {
    dealer.printCard();
  }

  void clearScreen() {
    print("\x1B[2J\x1B[0;0H");
  }

  void playerFunction() {
    createPlayerCard();
    showPlayerCard();
    showPlayerpoint();
  }

  void dealerFunction() {
    createDealerCard();
    showDealerCard();
    showDealerPoint();
  }

  int getDealerPoint() {
    return dealer.point;
  }

  int getPlayerPoint() {
    return player.point;
  }

  void resetArray() {
    player.playerCard = [];
    player.point = 0;
    dealer.dealerCard = [];
    dealer.point = 0;
  }

  void checkwin() {
    if (getDealerPoint() > getPlayerPoint() && getDealerPoint() < 22) {
      print('');
      print('Dealer win');
    } else if (getDealerPoint() == getPlayerPoint()) {
      print('');
      print('Draw');
    } else {
      print('');
      print('Player win');
    }
  }

  void decission() {
    stdout.write("[1]Hit [2]Stand : ");
    String choice = stdin.readLineSync()!;
    if (choice == '1') {
      clearScreen();
      showDealerCard();
      showDealerPoint();
      print('');
      playerFunction();
      sleep(const Duration(milliseconds: 1050));
      if (getPlayerPoint() > 21) {
        print('');
        print('Dealer win');
      } else {
        decission();
      }
    } else if (choice == '2') {
      clearScreen();
      dealerFunction();
      print('');
      showPlayerCard();
      showPlayerpoint();

      while (getDealerPoint() < 17) {
        sleep(const Duration(milliseconds: 2050));
        clearScreen();
        sleep(const Duration(milliseconds: 2050));
        dealerFunction();
        showPlayerCard();
        showPlayerpoint();
      }
      checkwin();
    } else {
      decission();
    }
  }

  void playGame() {
    resetArray();
    stdout.write("Dealer card :  ");
    dealerFunction();
    print('');

    stdout.write("Player card :  ");
    createPlayerCard();
    playerFunction();

    print('');
    decission();
    print('');
    stdout.write("You want to play more ? [1] Yes [2] No : ");
  }

  void runGame() {
    bool isEnd = false;
    while (isEnd != true) {
      playGame();
      String playmore = stdin.readLineSync()!;
      if (playmore == '2') {
        isEnd = true;
      } else {
        resetArray();
        clearScreen();
      }
    }
  }
}

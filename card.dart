import 'dart:ffi';
import 'dart:math';

class Card {
  Map card_values = {
    'A': 11,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 10,
    'J': 10,
    'Q': 10,
    'K': 10
  };

  int createCard(List<String> who) {
    int randomCard = Random().nextInt(card_values.length);
    int randomSuit = Random().nextInt(4);
    String space = ' ';
    if (randomCard == 9) {
      space = "";
    }
    String suit = '';

    if (randomSuit == 0) {
      suit = '♠';
    } else if (randomSuit == 1) {
      suit = '♦';
    } else if (randomSuit == 2) {
      suit = '♥';
    } else if (randomSuit == 3) {
      suit = '♣';
    }
    var key = card_values.keys.elementAt(randomCard);
    var value = card_values.values.elementAt(randomCard);

    String card =
        ('\n┌─────────┐\n│ ${key}${space}      │\n│         │\n|    ${suit}    |\n│         │\n│      ${key}${space} │\n└─────────┘');

    who.add(card);
    return value;
  }
}
